//----------------------------------------------------------------------------------------------------------------------
//
// Ethereum Virtual Machine Emulator
//
// This is not written for efficiency, but for hackability
// It's meant to be used for analyzing smart contracts
// It might be quite a memory hog! It traces the execution, every stack object and every byte of memory
//
// The idea is to create an execution graph and a data graph for analysis
//
// Author: John Short <john@short.digital>
// Copyright © 2017 - 2019
// License: GPLv3
//
//----------------------------------------------------------------------------------------------------------------------

const keccak256 = require('js-sha3').keccak256
const EtherValue = require('./ethervalue.js')

//---------------------------------------------------------------------------------------------------------------------
//
// Gas
//
//---------------------------------------------------------------------------------------------------------------------
// Gas prices
const gas = {
	// Non-opcode gas prices
	GDEFAULT: 1,
	GMEMORY: 3,
	GQUADRATICMEMDENOM: 512,  // 1 gas per 512 quadwords
	GEXPONENTBYTE: 10,        // cost of EXP exponent per byte
	GCOPY: 3,                 // cost to copy one 32 byte word
	GCONTRACTBYTE: 200,       // one byte of code in contract creation
	GCALLVALUETRANSFER: 9000, // non-zero-valued call
	GLOGBYTE: 8,              // cost of a byte of logdata
	GTXCOST: 21000,           // TX BASE GAS COST
	GTXDATAZERO: 4,           // TX DATA ZERO BYTE GAS COST
	GTXDATANONZERO: 68,       // TX DATA NON ZERO BYTE GAS COST
	GSHA3WORD: 6,             // Cost of SHA3 per word
	GSHA256BASE: 60,          // Base c of SHA256
	GSHA256WORD: 12,          // Cost of SHA256 per word
	GRIPEMD160BASE: 600,      // Base cost of RIPEMD160
	GRIPEMD160WORD: 120,      // Cost of RIPEMD160 per word
	GIDENTITYBASE: 15,        // Base cost of indentity
	GIDENTITYWORD: 3,         // Cost of identity per word
	GECRECOVER: 3000,         // Cost of ecrecover op
	GSTIPEND: 2300,
	GCALLNEWACCOUNT: 25000,
	GSUICIDEREFUND: 24000,
	GSTORAGEBASE: 2500,
	GSTORAGEBYTESTORAGE: 250,
	GSTORAGEBYTECHANGE: 40,
	GSTORAGEMIN: 2500,
	GSSIZE: 50,
	GSLOADBYTES: 50,
	GSTORAGEREFUND: 15000,
	GSTORAGEKILL: 5000,
	GSTORAGEMOD: 5000,
	GSTORAGEADD: 20000,
	GMODEXPQUADDIVISOR: 100,
	GECADD: 500,
	GECMUL: 2000,
	GPAIRINGBASE: 100000,
	GPAIRINGPERPOINT: 80000,
	EXP_SUPPLEMENTAL_GAS: 40,

	// Anti-DoS HF changes
	SLOAD_SUPPLEMENTAL_GAS: 150,
	CALL_SUPPLEMENTAL_GAS: 660,
	EXTCODELOAD_SUPPLEMENTAL_GAS: 680,
	BALANCE_SUPPLEMENTAL_GAS: 380,
	CALL_CHILD_LIMIT_NUM: 63,
	CALL_CHILD_LIMIT_DENOM: 64,
	SUICIDE_SUPPLEMENTAL_GAS: 5000,

	// Instruction base gas fee
	opcodes: {
		0x00: 0,     0x01: 3,  0x02: 5,     0x03: 3,    0x04: 5,    0x05: 5,  0x06: 5,  0x07: 5,  0x08: 8, 0x09: 8, 0x0a: 10, 0x0b: 5,
		0x10: 3,     0x11: 3,  0x12: 3,     0x13: 3,    0x14: 3,    0x15: 3,  0x16: 3,  0x17: 3,  0x18: 3, 0x19: 3, 0x1a: 3,
		0x20: 30,
		0x30: 2,     0x31: 20,  0x32: 2,    0x33: 2,    0x34: 2,    0x35: 3,  0x36: 2,  0x37: 3,  0x38: 2, 0x39: 3, 0x3a: 2,  0x3b: 20, 0x3c: 20, 0x3D: 2,
		0x40: 20,    0x41: 2,   0x42: 2,    0x43: 2,    0x44: 2,    0x45: 2,
		0x50: 2,     0x51: 3,   0x52: 3,    0x53: 3,    0x54: 50,   0x55: 0,  0x56: 8,  0x57: 10, 0x58: 2, 0x59: 2, 0x5a: 2,  0x5b: 1,
		0x60: 3,     0x61: 3,   0x62: 3,    0x63: 3,    0x64: 3,    0x65: 3,  0x66: 3,  0x67: 3,  0x68: 3, 0x69: 3, 0x6a: 3,  0x6b: 3, 0x6c: 3, 0x6d: 3, 0x6e: 3, 0x6f: 3,
		0x70: 3,     0x71: 3,   0x72: 3,    0x73: 3,    0x74: 3,    0x75: 3,  0x76: 3,  0x77: 3,  0x78: 3, 0x79: 3, 0x7a: 3,  0x7b: 3, 0x7c: 3, 0x7d: 3, 0x7e: 3, 0x7f: 3,
		0x80: 3,     0x81: 3,   0x82: 3,    0x83: 3,    0x84: 3,    0x85: 3,  0x86: 3,  0x87: 3,  0x88: 3, 0x89: 3, 0x8a: 3,  0x8b: 3, 0x8c: 3, 0x8d: 3, 0x8e: 3, 0x8f: 3,
		0x90: 3,     0x91: 3,   0x92: 3,    0x93: 3,    0x94: 3,    0x95: 3,  0x96: 3,  0x97: 3,  0x98: 3, 0x99: 3, 0x9a: 3,  0x9b: 3, 0x9c: 3, 0x9d: 3, 0x9e: 3, 0x9f: 3,
		0xa0: 375,   0xa1: 750, 0xa2: 1125, 0xa3: 1500, 0xa4: 1875,
		0xe1: 50,    0xe2: 0,   0xe3: 50,
		0xf0: 32000, 0xf1: 40,  0xf2: 40,   0xf3: 0,    0xf4: 40,   0xf5: 40, 0xfa: 40, 0xfd: 0, 
		0xfe: 0, // BRK - Not a real opcode! For debugging only (Useful when writing custom smart contracts)
		0xff: 0
	}
}

const opcodeMnmonics = {
	0x00: 'STOP',       0x01: 'ADD',      0x02: 'MUL',       0x03: 'SUB',         0x04: 'DIV',          0x05: 'SDIV',           0x06: 'MOD',            0x07: 'SMOD',
	0x08: 'ADDMOD',     0x09: 'MULMOD',   0x0A: 'EXP',       0x0B: 'SIGNEXTEND',
	0x10: 'LT',         0x11: 'GT',       0x12: 'SLT',       0x13: 'SGT',         0x14: 'EQ',           0x15: 'ISZERO',         0x16: 'AND',            0x17: 'OR',
	0x18: 'XOR',        0x19: 'NOT',      0x1A: 'BYTE',
	0x20: 'SHA3',
	0x30: 'ADDRESS',    0x31: 'BALANCE',  0x32: 'ORIGIN',    0x33: 'CALLER',      0x34: 'CALLVALUE',    0x35: 'CALLDATALOAD',   0x36: 'CALLDATASIZE',   0x37: 'CALLDATACOPY',
	0x38: 'CODESIZE',   0x39: 'CODECOPY', 0x3A: 'GASPRICE',  0x3B: 'EXTCODESIZE', 0x3C: 'EXTCODECOPY',  0x3D: 'RETURNDATASIZE', 0x3E: 'RETURNDATACOPY', 0x3F: 'EXTCODEHASH',
	0x40: 'BLOCKHASH',  0x41: 'COINBASE', 0x42: 'TIMESTAMP', 0x43: 'NUMBER',      0x44: 'DIFFICULTY',   0x45: 'GASLIMIT',
	0x50: 'POP',        0x51: 'MLOAD',    0x52: 'MSTORE',    0x53: 'MSTORE8',     0x54: 'SLOAD',        0x55: 'SSTORE',         0x56: 'JUMP',           0x57: 'JUMPI',
	0x58: 'PC',         0x59: 'MSIZE',    0x5A: 'GAS',       0x5B: 'JUMPDEST',
	0x60: 'PUSH1',      0x61: 'PUSH2',    0x62: 'PUSH3',     0x63: 'PUSH4',       0x64: 'PUSH5',        0x65: 'PUSH6',          0x66: 'PUSH7',          0x67: 'PUSH8',
	0x68: 'PUSH9',      0x69: 'PUSH10',   0x6A: 'PUSH11',    0x6B: 'PUSH12',      0x6C: 'PUSH13',       0x6D: 'PUSH14',         0x6E: 'PUSH15',         0x6F: 'PUSH16',
	0x70: 'PUSH17',     0x71: 'PUSH18',   0x72: 'PUSH19',    0x73: 'PUSH20',      0x74: 'PUSH21',       0x75: 'PUSH22',         0x76: 'PUSH23',         0x77: 'PUSH24',
	0x78: 'PUSH25',     0x79: 'PUSH26',   0x7A: 'PUSH27',    0x7B: 'PUSH28',      0x7C: 'PUSH29',       0x7D: 'PUSH30',         0x7E: 'PUSH31',         0x7F: 'PUSH32',
	0x80: 'DUP1',       0x81: 'DUP2',     0x82: 'DUP3',      0x83: 'DUP4',        0x84: 'DUP5',         0x85: 'DUP6',           0x86: 'DUP7',           0x87: 'DUP8',
	0x88: 'DUP9',       0x89: 'DUP10',    0x8A: 'DUP11',     0x8B: 'DUP12',       0x8C: 'DUP13',        0x8D: 'DUP14',          0x8E: 'DUP15',          0x8F: 'DUP16',
	0x90: 'SWAP1',      0x91: 'SWAP2',    0x92: 'SWAP3',     0x93: 'SWAP4',       0x94: 'SWAP5',        0x95: 'SWAP6',          0x96: 'SWAP7',          0x97: 'SWAP8',
	0x98: 'SWAP9',      0x99: 'SWAP10',   0x9A: 'SWAP11',    0x9B: 'SWAP12',      0x9C: 'SWAP13',       0x9D: 'SWAP14',         0x9E: 'SWAP15',         0x9F: 'SWAP16',
	0xA0: 'LOG0',       0xA1: 'LOG1',     0xA2: 'LOG2',      0xA3: 'LOG3',        0xA4: 'LOG4',
	0xF0: 'CREATE',     0xF1: 'CALL',     0xF2: 'CALLCODE',  0xF3: 'RETURN',      0xF4: 'DELEGATECALL', 0xF5: 'CALLBLACKBOX',
	0xFA: 'STATICCALL', 0xFD: 'REVERT',
	0xFE: 'BRK', // BRK - Not a real opcode! For debugging only
	0xFF: 'SELFDESTRUCT',
	0x100: 'PUSH'
}

//---------------------------------------------------------------------------------------------------------------------
//
// EtherAccount
//
//---------------------------------------------------------------------------------------------------------------------
class EtherAccount
{
	constructor()
	{
		
	}

	clone()
	{
		let a = new EtherAccount()
		if(this.address) a.setAddress(this.address)
		if(this.value) a.setValue(this.value)
		if(this.nonce) a.setNonce(this.nonce)
		if(this.code) a.setCode(this.code)
		if(this.storage) a.setStorage(this.storage.clone())
		return a
	}

	setAddress(address)
	{
		this.address = address
	}

	setValue(value)
	{
		this.value = value
	}

	setNonce(nonce)
	{
		this.nonce = nonce
	}

	setCode(code)
	{
		this.code = code
	}

	setStorage(storage)
	{
		this.storage = storage
	}
}

//---------------------------------------------------------------------------------------------------------------------
//
// EtherMemory
//
//---------------------------------------------------------------------------------------------------------------------
class EtherMemory
{
	constructor(evm, memory)
	{
		this.mem = []
		this._size = 0

		// Copy memory
		if(memory)
		{
			this.mem = new Uint8Array(memory.length)
			for(let i in memory)
				this.mem[i] = memory[i]
			this._size = this.mem.length
		}
		this.evm = evm

		this.writable = false
		this.growable = false
	}

	clone(evm)
	{
		let m = new EtherMemory(evm, this.mem)
		return m
	}

	size()
	{
		return this._size
	}

	setWritable(writable)
	{
		this.writable = writable
	}

	setGrowable(growable)
	{
		this.growable = growable
	}

	read(offset, size, cb)
	{
		let buf
		try
		{
			let offsetNum = offset
			if(offset instanceof EtherValue)
				offsetNum = offset.value()
			if(!(offsetNum instanceof BigInt))
				offsetNum = BigInt(offsetNum)

			let sizeNum = size
			if(size instanceof EtherValue)
				sizeNum = size.value()
			sizeNum = Number(sizeNum)

			let m = new Uint8Array(sizeNum)
			for(let i = 0n; i < sizeNum; i++)
			{
				let o = offsetNum + i
				if(o > this.mem._size)
					return cb(new Error("Offset out of bounds"))
				m[i] = this.mem[o]
			}
			buf = Buffer.from(m)
		}
		catch(e)
		{
			return cb(e, null)
		}
		if(cb)
			cb(null, buf)
		return buf
	}

	write(offset, data, size, cb)
	{
		if(!this.writable)
			return cb(new Error("Memory not writable"))

		let offsetNum = offset
		if(offsetNum instanceof EtherValue)
			offsetNum = offset.value()
		if(!(offsetNum instanceof BigInt))
			offsetNum = BigInt(offsetNum)

		let sizeNum = size
		if(size instanceof EtherValue)
			sizeNum = size.value()
		if(!(sizeNum instanceof BigInt))
			sizeNum = BigInt(sizeNum)

		let dataArr = data
		if(dataArr instanceof EtherValue)
			dataArr = data.toArray(sizeNum)

		for(let i = 0n; i < sizeNum; i++)
		{
			//console.log(typeof offsetNum)
			//console.log(offsetNum)
			let o = offsetNum + i
			if(o > this.mem.sizeNum && !this.growable)
				return cb(new Error("Offset out of bounds"))
			this.mem[o] = dataArr[i]
			if(o > this._size)
				this._size = o
		}
		if(cb)
			cb(null, null)
	}
}

//---------------------------------------------------------------------------------------------------------------------
//
// EtherStack
//
//---------------------------------------------------------------------------------------------------------------------
class EtherStack
{
	constructor()
	{
		this.stack = []
		this._size = 0
	}

	print()
	{
		let s = ''
		for(let i = 0; i < this._size; i++)
		{
			s += i+': '+this.stack[i].toString(32)+'\n'
		}
		return s
	}

	clone(evm)
	{
		let s = new EtherStack()
		// Cloning must be done in two steps: First cloning all the values
		// Then cleaning all the original nodes.
		// The value graph may contain circular references, so we cannot clone everything
		// in just 1 go.
		for(let i in this.stack)
			s.stack[i] = this.stack[i].clone(evm)
		for(let i in this.stack)
			this.stack[i].finishCloning()
		s._size = this._size
		return s
	}

	size()
	{
		return this._size
	}

	push(val)
	{
		this.stack.push(val)
		this._size++
	}

	set(index, value)
	{
		if(index >= this._size || index < 0)
			return new Error("Stack out of bounds")
		this.stack[(this._size - 1) - index] = value
	}

	tail(index)
	{
		if(index >= this._size || index < 0)
			return new Error("Stack out of bounds")
		else
			return this.stack[index]
	}

	head(index)
	{
		if(index >= this._size || index < 0)
			return new Error("Stack out of bounds")
		else
			return this.stack[(this._size - 1) - index]
	}

	pop()
	{
		this._size--
		return this.stack.pop()
	}
}

// Dummy storage
class DummyStorage
{
	constructor(evm)
	{
		this.storage = {}
		this.evm = evm
	}

	setEvm(evm)
	{
		this.evm = evm
	}

	load(values)
	{
		for(let i in values)
		{
			this.storage[i] = values[i]
		}
	}

	read(address, offset, cb)
	{
		let n = 0
		if(this.storage[offset.toString()])
			n = new EtherValue(this.storage[offset.toString()], this.evm)
		else
			n = new EtherValue(0, this.evm)
		n.setOrigin('storage', offset)
		cb(null, n)
	}

	write(address, offset, value, cb)
	{
		this.storage[offset.toString()] = value.toString()
		cb()
	}
}

//---------------------------------------------------------------------------------------------------------------------
//
// EthereumVirtualMachine
//
//---------------------------------------------------------------------------------------------------------------------
class EthereumVirtualMachine
{
	constructor(account)
	{
		this.account = account
		this.storage = new DummyStorage(this)
		this.memory = new EtherMemory(this)
		this.memory.setWritable(true)
		this.memory.setGrowable(true)
		this.stack = new EtherStack(this)

		this.calldata = new EtherMemory(this)
		this.calldata.setWritable(true)
		this.calldata.setGrowable(true)

		this.returndata = new EtherMemory(this)
		this.returndata.setWritable(true)
		this.returndata.setGrowable(true)

		this.pc = 0
		this.curPc = 0
		this.tick = 0
		this.pendingGas = 0
		this.accumulatedGas = 0

		this.dataId = 0
		this.dataTrace = []
		this.objTrace = []

		this.enableExecutionTrace = false
		this.enableDataTrace = false

		this.callvalue = new EtherValue(0, this)
		this.caller = new EtherValue(0, this)
		this.origin = new EtherValue(0, this)
		this.contractAddress = new EtherValue(0, this)
		this.code = new EtherMemory(this)
	}

	setWeb3(web3)
	{
		this.web3 = web3
	}

	addData(value)
	{
		if(value.id == -1)
			value.id = this.dataId++
		this.dataTrace.push(value)
	}

	setBlock(block)
	{
		this.block =
		{
			hash: new EtherValue(block.hash.substring(2), this),
			miner: new EtherValue(block.miner.substring(2), this),
			timestamp: new EtherValue(block.timestamp, this),
			number: new EtherValue(block.number, this),
			difficulty: new EtherValue(block.difficulty, this),
			gasLimit: new EtherValue(block.gasLimit, this),
		}
	}

	setCallvalue(value)
	{
		this.callvalue = new EtherValue(value, this)
	}

	setCaller(caller)
	{
		this.caller = new EtherValue(caller, this)
	}

	setOrigin(origin)
	{
		this.origin = new EtherValue(origin, this)
	}

	setContractAddress(address)
	{
		this.contractAddress = new EtherValue(address, this)
	}

	setCode(code)
	{
		this.code = new EtherMemory(this, code)
		this.code.setWritable(false) // :(
	}

	setCalldata(calldata)
	{
		this.calldata.write(0, calldata, calldata.length)
	}

	// Compute SHA3 hash
	hash(data)
	{
		return keccak256(data)
	}

	// Clones the entire VM plus stack
	// Useful for snapshotting when branching (So that both branches can be taken independently)
	clone()
	{
		let clonedAccount = undefined
		if(this.account)
			clonedAccount = this.account.clone(evm)
		let evm = new EthereumVirtualMachine(clonedAccount)
		evm.storage        = this.storage

		evm.pc             = this.pc
		evm.curPc          = this.curPc
		evm.tick           = this.tick
		evm.pendingGas     = this.pendingGas
		evm.accumulatedGas = this.accumulatedGas
		evm.dataId         = this.dataId

		evm.enableExecutionTrace = this.enableExecutionTrace
		evm.enableDataTrace = this.enableDataTrace

		evm.memory         = this.memory.clone(evm)
		evm.calldata       = this.calldata.clone(evm)
		evm.stack          = this.stack.clone(evm)

		/*if(this.callvalue)       */ evm.callvalue       = this.callvalue.clone(evm)
		/*if(this.caller)          */ evm.caller          = this.caller.clone(evm)
		/*if(this.origin)          */ evm.origin          = this.origin.clone(evm)
		/*if(this.contractAddress) */ evm.contractAddress = this.contractAddress.clone(evm)
		/*if(this.code)            */ evm.code            = this.code.clone(evm)

		for(let i in this.dataTrace)
			evm.dataTrace.push(this.dataTrace[i].clone(evm))

		return evm
	}

	setStorage(storage)
	{
		storage.setEvm(this)
		this.storage = storage
	}

	// Translate an opcode into an intrustion
	opcode(op)
	{
		switch(op)
		{
			case 0x00: return this.STOP;       case 0x01: return this.ADD;      case 0x02: return this.MUL;       case 0x03: return this.SUB;         case 0x04: return this.DIV;          case 0x05: return this.SDIV;           case 0x06: return this.MOD;            case 0x07: return this.SMOD;
			case 0x08: return this.ADDMOD;     case 0x09: return this.MULMOD;   case 0x0A: return this.EXP;       case 0x0B: return this.SIGNEXTEND;
			case 0x10: return this.LT;         case 0x11: return this.GT;       case 0x12: return this.SLT;       case 0x13: return this.SGT;         case 0x14: return this.EQ;           case 0x15: return this.ISZERO;         case 0x16: return this.AND;            case 0x17: return this.OR;
			case 0x18: return this.XOR;        case 0x19: return this.NOT;      case 0x1A: return this.BYTE;
			case 0x20: return this.SHA3;
			case 0x30: return this.ADDRESS;    case 0x31: return this.BALANCE;  case 0x32: return this.ORIGIN;    case 0x33: return this.CALLER;      case 0x34: return this.CALLVALUE;    case 0x35: return this.CALLDATALOAD;   case 0x36: return this.CALLDATASIZE;   case 0x37: return this.CALLDATACOPY;
			case 0x38: return this.CODESIZE;   case 0x39: return this.CODECOPY; case 0x3A: return this.GASPRICE;  case 0x3B: return this.EXTCODESIZE; case 0x3C: return this.EXTCODECOPY;  case 0x3D: return this.RETURNDATASIZE; case 0x3E: return this.RETURNDATACOPY; case 0x3F: return this.EXTCODEHASH;
			case 0x40: return this.BLOCKHASH;  case 0x41: return this.COINBASE; case 0x42: return this.TIMESTAMP; case 0x43: return this.NUMBER;      case 0x44: return this.DIFFICULTY;   case 0x45: return this.GASLIMIT;
			case 0x50: return this.POP;        case 0x51: return this.MLOAD;    case 0x52: return this.MSTORE;    case 0x53: return this.MSTORE8;     case 0x54: return this.SLOAD;        case 0x55: return this.SSTORE;         case 0x56: return this.JUMP;           case 0x57: return this.JUMPI;
			case 0x58: return this.PC;         case 0x59: return this.MSIZE;    case 0x5A: return this.GAS;       case 0x5B: return this.JUMPDEST;
			case 0x60: return this.PUSH1;      case 0x61: return this.PUSH2;    case 0x62: return this.PUSH3;     case 0x63: return this.PUSH4;       case 0x64: return this.PUSH5;        case 0x65: return this.PUSH6;          case 0x66: return this.PUSH7;          case 0x67: return this.PUSH8;
			case 0x68: return this.PUSH9;      case 0x69: return this.PUSH10;   case 0x6A: return this.PUSH11;    case 0x6B: return this.PUSH12;      case 0x6C: return this.PUSH13;       case 0x6D: return this.PUSH14;         case 0x6E: return this.PUSH15;         case 0x6F: return this.PUSH16;
			case 0x70: return this.PUSH17;     case 0x71: return this.PUSH18;   case 0x72: return this.PUSH19;    case 0x73: return this.PUSH20;      case 0x74: return this.PUSH21;       case 0x75: return this.PUSH22;         case 0x76: return this.PUSH23;         case 0x77: return this.PUSH24;
			case 0x78: return this.PUSH25;     case 0x79: return this.PUSH26;   case 0x7A: return this.PUSH27;    case 0x7B: return this.PUSH28;      case 0x7C: return this.PUSH29;       case 0x7D: return this.PUSH30;         case 0x7E: return this.PUSH31;         case 0x7F: return this.PUSH32;
			case 0x80: return this.DUP1;       case 0x81: return this.DUP2;     case 0x82: return this.DUP3;      case 0x83: return this.DUP4;        case 0x84: return this.DUP5;         case 0x85: return this.DUP6;           case 0x86: return this.DUP7;           case 0x87: return this.DUP8;
			case 0x88: return this.DUP9;       case 0x89: return this.DUP10;    case 0x8A: return this.DUP11;     case 0x8B: return this.DUP12;       case 0x8C: return this.DUP13;        case 0x8D: return this.DUP14;          case 0x8E: return this.DUP15;          case 0x8F: return this.DUP16;
			case 0x90: return this.SWAP1;      case 0x91: return this.SWAP2;    case 0x92: return this.SWAP3;     case 0x93: return this.SWAP4;       case 0x94: return this.SWAP5;        case 0x95: return this.SWAP6;          case 0x96: return this.SWAP7;          case 0x97: return this.SWAP8;
			case 0x98: return this.SWAP9;      case 0x99: return this.SWAP10;   case 0x9A: return this.SWAP11;    case 0x9B: return this.SWAP12;      case 0x9C: return this.SWAP13;       case 0x9D: return this.SWAP14;         case 0x9E: return this.SWAP15;         case 0x9F: return this.SWAP16;
			case 0xA0: return this.LOG0;       case 0xA1: return this.LOG1;     case 0xA2: return this.LOG2;      case 0xA3: return this.LOG3;        case 0xA4: return this.LOG4;
			case 0xF0: return this.CREATE;     case 0xF1: return this.CALL;     case 0xF2: return this.CALLCODE;  case 0xF3: return this.RETURN;      case 0xF4: return this.DELEGATECALL; case 0xF5: return this.CALLBLACKBOX;
			case 0xFA: return this.STATICCALL; case 0xFD: return this.REVERT;
			case 0xFE: return this.BRK; // BRK - Not a real opcode! For debugging only (Useful when writing custom smart contracts)
			case 0xFF: return this.SELFDESTRUCT;
			case 0x100: return this.PUSH; // Pseudo opcode - Never actually found in actual code
			default: return this.INVALID; // Pseudo opcode - Simply throws an error
		}
	}

	// Push value onto the stack
	push(value, cb)
	{
		this.stack.push(value)
		if(this.stack.size() > 1024)
		{
			let err = new Error("Stack overflow")
			if(cb)
				return cb(err)
			else
				this.error = err
		}
		if(cb)
			cb()
	}

	// Push n bytes to the stack
	pushStack(bytes, cb)
	{
		this.code.read(this.pc, bytes, (err, mem) =>
		{
			if(err) return cb(err, null)
			let v = new EtherValue(mem, this)
			//console.log(v)
			v.setOrigin('push')
			this.push(v, (err, res) =>
			{
				this.pc += bytes
				cb(err, res)
			})
		})
	}

	duplicateStack(index, cb)
	{
		let e = this.stack.head(index - 1)
		if(e instanceof Error)
			return cb(e)
		this.stack.push(e.dup())
		cb()
	}

	swapStack(index, cb)
	{
		let e = this.stack.head(index)
		let f = this.stack.head(0)
		if(e instanceof Error)
			return cb(e)
		if(f instanceof Error)
			return cb(f)
		this.stack.set(0, e)
		this.stack.set(index, f)
		cb()
	}

	getOpcodeStackItems(op)
	{
		let func = this.opcode(op)
		let argc = func.length - 1
		let stack = []
		for(let i = 0; i < argc; i++)
			stack.push(this.stack.head(i))
		return stack
	}

	// Take an opcode and execute the corresponding instruction
	// This also automagically pops all the required stack elements
	execute(op, cb)
	{
		let func = this.opcode(Number(op))
		if(!func)
		{
			throw new Error('Unknown opcode: 0x'+op.toString(16))
		}
		let argc = func.length
		let argv = []
		let stack = []
		this.pendingGas = gas.opcodes[op] || 0
		for(let i = 0; i < argc - 1; i++)
		{
			let s = this.stack.pop()
			if(s == null)
			{
				return cb(new Error("Stack underflow"))
			}
			s.kill() // Popping kills (Doesn't affect DUP or SWAP instructions)
			argv.push(s)
			stack.push(s)
		}
		((op) =>
		{

			let oldStackSize = this.stack.size()
			argv.push((err) =>
			{
				if(this.error) err = this.error
				if(err) this.error = err

				this.accumulatedGas += this.pendingGas

				// TODO: Check if out-of-gas exception needs to be throw
				// ...

				let newStackSize = this.stack.size() - oldStackSize
				let newStack = []
				for(let i = 0; i < newStackSize; i++)
					newStack.push(this.stack.head(i))

				let o = {
					stack: stack,
					newStack: newStack,
					func: func,
					err: err,
					gasUsed: this.pendingGas,
					accumulatedGas: this.accumulatedGas,
				}

				cb(err, o)
			})
			func.apply(this, argv)
		})(op)
	}

	opcodeToMnemonic(op)
	{
		return opcodeMnmonics[op]
	}

	// Read the next instruction, but don't execute
	peekNextInstruction(cb)
	{
		this.code.read(this.pc, 1, (err, op) =>
		{
			if(err) return cb(err, null)
			cb(null, op[0])
		})
	}

	willBranch(cb)
	{
		this.code.read(this.pc, 1, (err, op) =>
		{
			if(err) return cb(err, null)

			op = op[0]

			if(opcodeMnmonics[op] == 'JUMP')
				return cb(null, true)

			if(opcodeMnmonics[op] != 'JUMPI')
				return cb(null, false)

			let topStack = this.stack.head(0)
			if(!topStack.eq(0))
				return cb(null, true)
			return cb(null, false)
		})
	}

	// Read next instruction, execute it and call back
	step(cb)
	{
		if(this.stopped)
			return cb(new Error('VM stopped'))
		this.code.read(this.pc, 1, (err, op) =>
		{
			if(err) return cb(err, null)
			this.curPc = this.pc
			this.pc++
			let opcode = op[0]
			this.execute(opcode, (err, res) =>
			{
				if(this.enableExecutionTrace)
				{
					let traceObj = {pc: this.pc-1, opcode: opcode, mnemonic: opcodeMnmonics[opcode] || 'INVALID', tick: this.tick, gas: res.gasUsed, gasUsed: res.accumulatedGas, remainingGas: 0}
					traceObj.consumedStack = res.stack
					traceObj.addedStack = res.newStack
					/*
					console.log(
						(''+traceObj.tick).padStart(3, '0')+' - '+
						(''+traceObj.pc).padStart(4, '0')+' '+
						traceObj.mnemonic.padEnd(12, ' ')+' '+
						(''+traceObj.gas).padStart(5, ' ')+' '+
						(''+traceObj.gasUsed).padStart(6, ' ')+' '+
						(''+traceObj.remainingGas).padStart(6, ' ')
					)
					*/
					this.objTrace.push(traceObj);
				}
				this.tick++
				cb(err, res)
			})
		})
	}

	//----------------------------------------------------------------------------------------------------------
	//
	// INSUTRCTIONS
	// (The rest of this class doesn't contain anything particularily interesting...)
	//
	//----------------------------------------------------------------------------------------------------------
	//
	// An instruction can throw an error in 1 of 2 ways:
	// 1) Set the this.error field
	// 2) Set an error as the first callback argument
	//
	// Both methods are perfectly valid.
	// If multiple errors are thrown, then this.error will take precedence over cb(err)
	//

	// ALPHA: Stack items produced
	// DELTA: Stack items consumed
	// HEX  OP     DELTA  ALPHA  DESCRIPTION (Taken from the yellow paper)

	// Pseudo opcode for invalid opcodes
	INVALID(cb)
	{
		cb(new Error("Invalid instruction"))
	}

	//----------------------------------------------------------------------------------------------------------
	//
	// 0x00 - Stop and Arithmetic Operations
	//
	//----------------------------------------------------------------------------------------------------------
	// 0x00 STOP - D: 0 - A: 0 - Halts execution.
	STOP(cb)
	{
		this.stopped = 1
		cb()
	}

	// 0x01 ADD - D: 2 - A: 1 - Addition operation.
	ADD(left, right, cb)
	{
		this.push(left.add(right), cb)
	}

	// 0x02 MUL - D: 2 - A: 1 - Multiplication operation.
	MUL(multiplier, multiplicand, cb)
	{
		this.push(multiplier.mul(multiplicand), cb)
	}

	// 0x03 SUB - D: 2 - A: 1 - Subtraction operation.
	SUB(left, right, cb)
	{
		this.push(left.sub(right), cb)
	}

	// 0x04 DIV - D: 2 - A: 1 - Integer division operation. (Returns 0 if s[1] is 0)
	DIV(dividend, divisor, cb)
	{
		this.push(dividend.div(divisor), cb)
	}

	// 0x05 SDIV - D: 2 - A: 1 - Signed integer division operation (truncated).
	SDIV(dividend, divisor, cb)
	{
		// WARN: Unfinished
		return cb(new Error("Unfinished instruction: SDIV"))
		this.push(dividend.div(divisor))
	}

	// 0x06 MOD - D: 2 - A: 1 - Modulo remainder operation.
	MOD(dividend, divisor, cb)
	{
		this.push(dividend.mod(divisor), cb)
	}

	// 0x07 SMOD - D: 2 - A: 1 - Signed modulo remainder operation.
	SMOD(dividend, divisor, cb)
	{
		// WARN: Unfinished
		return cb(new Error("Unfinished instruction: SMOD"))
		this.push(dividend.mod(divisor))
	}

	// 0x08 ADDMOD - D: 3 - A: 1 - Modulo addition operation.
	// All intermediate calculations of this operation are not subject to the 2^256 modulo.
	ADDMOD(left, right, divisor, cb)
	{
		cb(new Error("Unfinished instruction: ADDMOD"))
	}

	// 0x09 MULMOD - D: 3 - A: 1 - Modulo multiplication operation.
	// All intermediate calculations of this operation are not subject to the 2^256 modulo.
	MULMOD(multiplier, multiplicand, divisor, cb)
	{
		cb(new Error("Unfinished instruction: MULMOD"))
	}

	// 0x0A EXP - D: 2 - A: 1 - Exponential operation.
	EXP(base, exponent, cb)
	{
		let res
		try
		{
			res = base.exp(exponent)
		}
		catch(e)
		{
			res = base
		}
		let n = exponent.size()
		this.pendingGas += /*(n * gas.EXP_SUPPLEMENTAL_GAS) +*/ (n * gas.GEXPONENTBYTE)
		this.push(res, cb)
	}

	// 0x0B SIGNEXTEND - D: 2 - A: 1 - Extend length of two’s complement signed integer.
	SIGNEXTEND(value, bits, cb)
	{
		this.push(signExtend(value, bits), cb)
	}

	//----------------------------------------------------------------------------------------------------------
	//
	// 0x10 - Comparison & Bitwise Logic Operations
	//
	//----------------------------------------------------------------------------------------------------------
	// 0x10 LT - D: 2 - A: 1 - Less-than comparision.
	LT(left, right, cb)
	{
		this.push(left.lt(right), cb)
	}

	// 0x11 GT - D: 2 - A: 1 - Greater-than comparision.
	GT(left, right, cb)
	{
		this.push(left.gt(right), cb)
	}

	// 0x12 SLT - D: 2 - A: 1 - Signed less-than comparision.
	// Where all values are treated as two’s complement signed 256-bit integers.
	SLT(left, right, cb)
	{
		// WARNING: Unfinished
		this.push(left.lt(right), cb)
	}

	// 0x13 SGT - D: 2 - A: 1 - Signed greater-than comparision.
	// Where all values are treated as two’s complement signed 256-bit integers.
	SGT(left, right, cb)
	{
		this.push(left.gt(right), cb)
	}

	// 0x14 EQ - D: 2 - A: 1 - Equality comparision.
	EQ(left, right, cb)
	{
		this.push(left.eq(right), cb)
	}
	
	// 0x15 ISZERO - D: 1 - A: 1 - Simple not operator.
	ISZERO(value, cb)
	{
		this.push(value.isZero(), cb)
	}

	// 0x16 AND - D: 2 - A: 1 - Bitwise AND operation
	AND(left, right, cb)
	{
		this.push(left.and(right), cb)
	}

	// 0x17 OR - D: 2 - A: 1 - Bitwise OR operation.
	OR(left, right, cb)
	{
		this.push(left.or(right), cb)
	}

	// 0x18 XOR - D: 2 - A: 1 - Bitwise XOR operation.
	XOR(left, right, cb)
	{
		this.push(left.xor(right), cb)
	}

	// 0x19 NOT - D: 1 - A: 1 - Bitwise NOT operation.
	NOT(value, cb)
	{
		this.push(value.not(), cb)
	}

	// 0x1A BYTE - D: 2 - A: 1 - Retrieve single byte from word.
	// For Nth byte, we count from the left (i.e. N=0 would be the most significant in big endian).
	BYTE(cb)
	{
		cb(new Error("Unfinished instruction: BYTE"))
	}
	
	// 0x20 SHA3 - D: 2 - A: 1 - Compute Keccak-256 hash.
	SHA3(offset, length, cb)
	{
		let data = this.memory.read(offset, length)
		let hash = new EtherValue(this.hash(data), this)
		hash.setOrigin('hash', offset, length)
		this.pendingGas += (this.ceil32(data.length) >> 5) * gas.GSHA3WORD
		this.push(hash, cb)
	}

	//----------------------------------------------------------------------------------------------------------
	//
	// 0x30 - Environmental Information
	//
	//----------------------------------------------------------------------------------------------------------
	// 0x30 ADDRESS - D: 0 - A: 1 - Get address of currently executing account.
	ADDRESS(cb)
	{
		let n
		if(this.block && this.block.miner)
			n = new EtherValue(this.account.address, this)
		else
			n = new EtherValue(0, this)
		n.setOrigin('address')
		this.push(n, cb)
	}

	// 0x31 BALANCE - D: 1 - A: 1 - Get balance of the given account.
	BALANCE(address, cb)
	{
		let n = new EtherValue(0, this)
		n.setOrigin('balance', address)
		address.addChild(n)
		this.push(n, cb)
		return
		return cb(new Error("Unfinished instruction: BALANCE"))
		console.log(new Error('test'))
		console.log('BALANCE 0x'+s[0].toString(20))
		this.ethereum.getBalance('0x'+s[0].toString(20), this.block.number, (balance) =>
		{
			console.log(balance)
			let n = new EtherValue(balance, this)
			n.setOrigin('balance', address)
			this.push(n, cb)
		})
	}

	// 0x32 ORIGIN - D: 0 - A: 1 - Get execution origination address.
	// This is the sender of original transaction; it is never an account with non-empty associated code.
	ORIGIN(cb)
	{
		let n
		if(this.block && this.block.miner)
			n = new EtherValue(this.origin, this)
		else
			n = new EtherValue(0, this)
		n.setOrigin('origin')
		this.push(n, cb)
	}

	// 0x33 CALLER - D: 0 - A: 1 - Get caller address.
	// This is the address of the account that is directly responsible for this execution.
	CALLER(cb)
	{
		let n = new EtherValue(this.caller, this)
		n.setOrigin('caller')
		this.push(n, cb)
	}

	// 0x34 CALLVALUE - D: 0 - A: 1 - Get deposited value by the instruction/transaction responsible for this execution.
	CALLVALUE(cb)
	{
		let n = new EtherValue(this.callvalue, this)
		n.setOrigin('callvalue')
		this.push(n, cb)
	}

	// 0x35 CALLDATALOAD - D: 1 - A: 1 - Get input data of current environment.
	// This pertains to the input data passed with the message call instruction or transaction.
	CALLDATALOAD(offset, cb)
	{
		//this.calldataLoads[printHex(offset.toNumber())] = true
		let mem = this.calldata.read(offset, 32)
		let n = new EtherValue(mem, this)
		n.setOrigin('calldataload', offset)
		this.push(n, cb)
	}

	// 0x36 CALLDATASIZE - D: 0 - A: 1 - Get size of input data in current environment.
	// This pertains to the input data passed with the message call instruction or transaction.
	CALLDATASIZE(cb)
	{
		let n = new EtherValue(this.calldata.size(), this)
		n.setOrigin('calldatasize')
		this.push(n, cb)
	}

	// 0x37 CALLDATACOPY - D: 3 - A: 0 - Copy input data in current environment to memory.
	// This pertains to the input data passed with the message call instruction or transaction.
	CALLDATACOPY(destination, source, length, cb)
	{
		//return cb(new Error("Fix me"))
		this.pendingGas += this.copyDataFee(length)
		this.calldata.read(source, length, (err, data) =>
		{
			if(err) return cb(err)
			this.memory.write(destination, data, length, (err) =>
			{
				if(err) return cb(err)
				cb()
			})
		})
	}

	// 0x38 CODESIZE - D: 0 - A: 1 - Get size of code running in current environment.
	CODESIZE(cb)
	{
		let n = new EtherValue(this.code.size(), this)
		n.setOrigin('codesize')
		this.push(n, cb)
	}

	// 0x39 CODECOPY - D: 3 - A: 0 - Copy code running in current environment to memory.
	CODECOPY(destination, length, source, cb)
	{
		this.pendingGas += this.copyDataFee(length)
		this.code.read(source, length, (err, data) =>
		{
			if(err) return cb(err)
			this.memory.write(destination, data, length, (err) =>
			{
				if(err) return cb(err)
				cb()
			})
		})
	}

	// 0x3A GASPRICE - D: 0 - A: 1 - Get price of gas in current environment.
	// This is gas price specified by the originating transaction.
	GASPRICE(cb)
	{
		let n = new EtherValue(this.gasPrice, this)
		n.setOrigin('gasprice')
		this.push(n, cb)
	}

	// 0x3B EXTCODESIZE - D: 1 - A: 1 - Get size of an account’s code.
	EXTCODESIZE(address, cb)
	{
		console.log("Warning: Unfinished instruction: EXTCODESIZE")
		let n = new EtherValue(0, this)
		n.setOrigin('extcodesize', address)
		address.addChild(n)
		this.push(n, cb)
		return
		//cb(new Error("Unfinished instruction: EXTCODESIZE"))
	}

	// 0x3C EXTCODECOPY - D: 4 - A: 0 - Copy an account’s code to memory.
	EXTCODECOPY(address, destination, source, length, cb)
	{
		cb(new Error("Unfinished instruction: EXTCODECOPY"))
	}

	// 0x3D RETURNDATASIZE - D: 0 - A: 1 - Get size of returned data.
	RETURNDATASIZE(cb)
	{
		// unfinished
		let n = new EtherValue(0, this)
		n.setOrigin('returndatasize')
		this.push(n, cb)
	}

	// 0x3E CALLDATACOPY - D: 3 - A: 0 - Copy return data from previous call.
	RETURNDATACOPY(destination, source, length, cb)
	{
		//return cb(new Error("Fix me"))
		this.pendingGas += this.copyDataFee(length)
		this.returndata.read(source, length, (err, data) =>
		{
			if(err) return cb(err)
			this.memory.write(destination, data, length, (err) =>
			{
				if(err) return cb(err)
				cb()
			})
		})
	}

	//----------------------------------------------------------------------------------------------------------
	//
	// 0x40 - Block Information
	//
	//----------------------------------------------------------------------------------------------------------
	// 0x40 BLOCKHASH - D: 1 - A: 1 - Get the hash of one of the 256 most recent complete blocks.
	// 0 is left on the stack if the looked for block number is greater than the current block number or more than 256 blocks behind the current block.
	BLOCKHASH(cb)
	{
		let n = new EtherValue(this.block.hash, this)
		n.setOrigin('blockhash')
		this.push(n, cb)
	}

	// 0x41 COINBASE - D: 0 - A: 1 - Get the block’s beneficiary address.
	COINBASE(cb)
	{
		let n
		if(this.block && this.block.miner)
			n = new EtherValue(this.block.miner, this)
		else
			n = new EtherValue(0, this)
		n.setOrigin('coinbase')
		this.push(n, cb)
	}

	// 0x42 TIMESTAMP - D: 0 - A: 1 - Get the block’s timestamp.
	TIMESTAMP(cb)
	{
		let n
		if(this.block && this.block.miner)
			n = new EtherValue(this.block.timestamp, this)
		else
			n = new EtherValue(0, this)
		n.setOrigin('timestamp')
		this.push(n, cb)
	}

	// 0x43 NUMBER - D: 0 - A: 1 - Get the block’s number.
	NUMBER(cb)
	{
		let n = new EtherValue(this.block.number, this)
		n.setOrigin('number')
		this.push(n, cb)
	}

	// 0x44 DIFFICULTY - D: 0 - A: 1 - Get the block’s difficulty.
	DIFFICULTY(cb)
	{
		let n = new EtherValue(this.block.difficulty, this)
		n.setOrigin('difficulty')
		this.push(n, cb)
	}

	// 0x45 GASLIMIT - D: 0 - A: 1 - Get the block’s gas limit.
	GASLIMIT(cb)
	{
		let n = new EtherValue(this.block.gasLimit, this)
		n.setOrigin('gaslimit')
		this.push(n, cb)
	}

	//----------------------------------------------------------------------------------------------------------
	//
	// 0x50 - Stack, Memory, Storage and Flow Operations
	//
	//----------------------------------------------------------------------------------------------------------
	// 0x50 POP - D: 1 - A: 0 - Remove item from stack.
	POP(foo, cb)
	{
		cb()
	}
	
	// 0x51 MLOAD - D: 1 - A: 1 - Load word from memory.
	MLOAD(offset, cb)
	{
		this.memory.read(offset, 32, (err, mem) =>
		{
			//console.log(mem)
			let n = new EtherValue(mem, this)
			n.setOrigin('mload', offset)
			this.push(n, cb)
		})
	}

	// 0x52 MSTORE - D: 2 - A: 0 - Save word to memory.
	MSTORE(offset, value, cb)
	{
		let oldSize = this.memory.size()
		this.memory.write(offset, value, 32, (err, res) =>
		{
			if(err) return cb(err)
			let newSize = this.memory.size()
			this.pendingGas += this.calculateMemoryFee(oldSize, newSize)
			cb()
		})
	}

	// 0x53 MSTORE8 - D: 2 - A: 0 - Save byte to memory.
	MSTORE8(offset, value, cb)
	{
		let oldSize = this.memory.size()
		this.memory.write(offset, value.and(new EtherValue(0xFF, this)), 1, (err, res) =>
		{
			if(err) return cb(err)
			let newSize = this.memory.size()
			this.pendingGas += this.calculateMemoryFee(oldSize, newSize)
			cb()
		})
	}

	// 0x54 SLOAD - D: 1 - A: 1 - Load word from storage.
    SLOAD(offset, cb)
	{
		this.storage.read(this.contractAddress, offset, (err, val) =>
		{
			if(err) return cb(err)
			let n = new EtherValue(val, this)
			n.setOrigin('sload', offset)
			this.push(n, cb)
		})
	}

	// 0x55 SSTORE - D: 2 - A: 0 - Save word to storage.
	SSTORE(offset, value, cb)
	{
		// First we much check what we're overwriting
		this.storage.read(this.contractAddress, offset, (err, val) =>
		{
			if(err) return cb(err)
			if(val.eq(0).value())
			{
				if(!new EtherValue(0, this).eq(value))
					this.pendingGas += gas.GSTORAGEADD
				else
					this.pendingGas += gas.GSTORAGEMOD
			}
			else if(value.eq(0).value())
				this.gasRefund += gas.GSTORAGEREFUND
			else
				this.pendingGas += gas.GSTORAGEMOD
			this.storage.write(this.contractAddress, offset, value, (err) =>
			{
				if(err) return cb(err)
				cb()
			})
		})
	}

	// 0x56 JUMP - D: 1 - A: 0 - Alter the program counter.
	JUMP(offset, cb)
	{
		this.pc = Number(offset.value())
		this.jumped = 1
		cb()
	}

	// 0x57 JUMPI - D: 2 - A: 0 - Conditionally alter the program counter.
	JUMPI(offset, value, cb)
	{
		if(!value.eq(0).value())
		{
			//console.log("Made jump")
			this.pc = Number(offset.value())
			this.jumped = 1
			cb()
		}
		else
			cb()
	}

	// 0x58 PC - D: 0 - A: 1 - Get the value of the program counter prior to the increment corresponding to this instruction.
	PC(cb)
	{
		let n = new EtherValue(this.pc, this)
		n.setOrigin('pc')
		this.push(n, cb)
	}

	// 0x59 MSIZE - D: 0 - A: 1 - Get the size of active memory in bytes.
	MSIZE(cb)
	{
		let n = new EtherValue(this.memory.length, this)
		n.setOrigin('msize')
		this.push(n, cb)
	}

	// 0x5A GAS - D: 0 - A: 1 - Get the amount of available gas, including the corresponding reduction for the cost of this instruction.
	GAS(cb)
	{
		let n = new EtherValue(this.gas, this)
		n.setOrigin('gas')
		this.push(n, cb)
	}

	// 0x5B JUMPDEST - D: 0 - A: 0 - Mark a valid destination for jumps.
	// This operation has no effect on machine state during execution.
	JUMPDEST(cb)
	{
		this.jumped = 0
		cb()
	}

	//----------------------------------------------------------------------------------------------------------
	//
	// 0x60 - 0x70 - Push Operations
	//
	//----------------------------------------------------------------------------------------------------------
	PUSH1(cb){  this.pushStack( 1, cb) }
	PUSH2(cb){  this.pushStack( 2, cb) }
	PUSH3(cb){  this.pushStack( 3, cb) }
	PUSH4(cb){  this.pushStack( 4, cb) }
	PUSH5(cb){  this.pushStack( 5, cb) }
	PUSH6(cb){  this.pushStack( 6, cb) }
	PUSH7(cb){  this.pushStack( 7, cb) }
	PUSH8(cb){  this.pushStack( 8, cb) }
	PUSH9(cb){  this.pushStack( 9, cb) }
	PUSH10(cb){ this.pushStack(10, cb) }
	PUSH11(cb){ this.pushStack(11, cb) }
	PUSH12(cb){ this.pushStack(12, cb) }
	PUSH13(cb){ this.pushStack(13, cb) }
	PUSH14(cb){ this.pushStack(14, cb) }
	PUSH15(cb){ this.pushStack(15, cb) }
	PUSH16(cb){ this.pushStack(16, cb) }
	PUSH17(cb){ this.pushStack(17, cb) }
	PUSH18(cb){ this.pushStack(18, cb) }
	PUSH19(cb){ this.pushStack(19, cb) }
	PUSH20(cb){ this.pushStack(20, cb) }
	PUSH21(cb){ this.pushStack(21, cb) }
	PUSH22(cb){ this.pushStack(22, cb) }
	PUSH23(cb){ this.pushStack(23, cb) }
	PUSH24(cb){ this.pushStack(24, cb) }
	PUSH25(cb){ this.pushStack(25, cb) }
	PUSH26(cb){ this.pushStack(26, cb) }
	PUSH27(cb){ this.pushStack(27, cb) }
	PUSH28(cb){ this.pushStack(28, cb) }
	PUSH29(cb){ this.pushStack(29, cb) }
	PUSH30(cb){ this.pushStack(30, cb) }
	PUSH31(cb){ this.pushStack(31, cb) }
	PUSH32(cb){ this.pushStack(32, cb) }

	//----------------------------------------------------------------------------------------------------------
	//
	// 0x80 - Duplication Operations
	//
	//----------------------------------------------------------------------------------------------------------
	DUP1(cb){  this.duplicateStack( 1, cb) }
	DUP2(cb){  this.duplicateStack( 2, cb) }
	DUP3(cb){  this.duplicateStack( 3, cb) }
	DUP4(cb){  this.duplicateStack( 4, cb) }
	DUP5(cb){  this.duplicateStack( 5, cb) }
	DUP6(cb){  this.duplicateStack( 6, cb) }
	DUP7(cb){  this.duplicateStack( 7, cb) }
	DUP8(cb){  this.duplicateStack( 8, cb) }
	DUP9(cb){  this.duplicateStack( 9, cb) }
	DUP10(cb){ this.duplicateStack(10, cb) }
	DUP11(cb){ this.duplicateStack(11, cb) }
	DUP12(cb){ this.duplicateStack(12, cb) }
	DUP13(cb){ this.duplicateStack(13, cb) }
	DUP14(cb){ this.duplicateStack(14, cb) }
	DUP15(cb){ this.duplicateStack(15, cb) }
	DUP16(cb){ this.duplicateStack(16, cb) }

	//----------------------------------------------------------------------------------------------------------
	//
	// 0x90 - Exchange Operations
	//
	//----------------------------------------------------------------------------------------------------------
	SWAP1(cb){  this.swapStack( 1, cb) }
	SWAP2(cb){  this.swapStack( 2, cb) }
	SWAP3(cb){  this.swapStack( 3, cb) }
	SWAP4(cb){  this.swapStack( 4, cb) }
	SWAP5(cb){  this.swapStack( 5, cb) }
	SWAP6(cb){  this.swapStack( 6, cb) }
	SWAP7(cb){  this.swapStack( 7, cb) }
	SWAP8(cb){  this.swapStack( 8, cb) }
	SWAP9(cb){  this.swapStack( 9, cb) }
	SWAP10(cb){ this.swapStack(10, cb) }
	SWAP11(cb){ this.swapStack(11, cb) }
	SWAP12(cb){ this.swapStack(12, cb) }
	SWAP13(cb){ this.swapStack(13, cb) }
	SWAP14(cb){ this.swapStack(14, cb) }
	SWAP15(cb){ this.swapStack(15, cb) }
	SWAP16(cb){ this.swapStack(16, cb) }

	//----------------------------------------------------------------------------------------------------------
	//
	// 0xA0 - Logging Operations
	//
	//----------------------------------------------------------------------------------------------------------
	// We don't care about logs... Just subtract the gas it uses
	// This is a stupid function anyway. Just browse the block chain!
	LOG0(a, b, cb)            { this.pendingGas += Number(b.value()) * gas.GLOGBYTE; cb() }
	LOG1(a, b, c, cb)         { this.pendingGas += Number(b.value()) * gas.GLOGBYTE; cb() }
	LOG2(a, b, c, d, cb)      { this.pendingGas += Number(b.value()) * gas.GLOGBYTE; cb() }
	LOG3(a, b, c, d, e, cb)   { this.pendingGas += Number(b.value()) * gas.GLOGBYTE; cb() }
	LOG4(a, b, c, d, e, f, cb){ this.pendingGas += Number(b.value()) * gas.GLOGBYTE; cb() }

	//----------------------------------------------------------------------------------------------------------
	//
	// 0xF0 - System operations
	//
	//----------------------------------------------------------------------------------------------------------
	// 0xF0 CREATE - D: 3 - A: 1 - Create a new account with associated code.
	CREATE(value, offset, length, cb)
	{
		//message(from, to, value, gas, gasPrice, expectedGas, nonce, callData, depth, callback)
		if(!this.ethereum)
			return cb(new Error("Missing Ethereum environment"))
		this.ethereum.message(
			this.account.address,
			null,
			value,
			this.gas,
			this.gasPrice,
			0,
			this.account.nonce,
			this.readMemBlock(this.memory, offset, length),
			this.depth + 1,
			true,
			(err, res) =>
		{
			this.account.changed = true
			this.account.nonce = this.account.nonce == null ? 1 : this.account.nonce + 1
			this.pendingGas += res.gasUsed
			this.push(this.val('0x'+res.address))
			/*
			let contract = {
				address: res.address,
				txhash: null,
				blockNumber: this.block.number,
				code: res.return,
				balance: res.balance,
				state: 'alive',
				nonce: res.nonce,
				analysis: {}
			}
			if(res.subContracts)
				contract.subContracts = res.subContracts;
			*/
			this.subContracts.push({err,res})
			cb()
		})
	}

	// call(gas, address, value, in, insize, out, outsize)
	// call contract at address a with input mem[in..(in+insize)) providing g gas and v wei and output area mem[out..(out+outsize))
	// returning 0 on error (eg. out of gas) and 1 on success
	// 0xF1 CALL - D: 7 - A: 1 - Message-call into an account.
	CALL(gas, address, value, in_, insize, out, outsize, cb)
	{
		console.log("Warning - Unfinished instruction: CALL")
		console.log("Gas:     "+gas.toString())
		console.log("Address: "+address.toString())
		console.log("Value:   "+value.toString())
		console.log("In:      "+in_.toString())
		console.log("Insize:  "+insize.toString())
		console.log("Out:     "+out.toString())
		console.log("Outsize: "+outsize.toString())
		//console.log(address.toString())
		//cb(new Error("Unfinished instruction: CALL"))

		let n = new EtherValue(1, this)
		n.setOrigin('call')
		this.push(n, cb)
	}

	// 0xF2 CALLCODE - D: 7 - A: 1 - Message-call into this account with an alternative account’s code.
	CALLCODE(cb)
	{
		cb(new Error("Unfinished instruction: CALLCODE"))
	}

	// 0xF3 RETURN - D: 2 - A: 0 - Halt execution returning output data.
	RETURN(offset, length, cb)
	{
		this.memory.read(offset, length, (err, data) =>
		{
			this.return = data
			this.stopped = 1
			cb()
		})
	}

	// 0xF4 DELEGATECALL - D: 6 - A: 1 - Message-call into this account with an alternative account’s code, but persisting the current values for sender and value.
	DELEGATECALL(cb)
	{
		cb(new Error("Unfinished instruction: DELEGATECALL"))
	}

	// 0xF5 CALLBLACKBOX - D: 0 - A: 0 - ???
	CALLBLACKBOX(cb)
	{
		cb(new Error("Unfinished instruction: CALLBLACKBOX"))
	}

	// 0xFA STATICCALL - D: 0 - A: 0 - ???
	STATICCALL(cb)
	{
		cb(new Error("Unfinished instruction: STATICCALL"))
	}

	// 0xFD REVERT - D: 2 - A: 0 - Halt execution reverting state changes but returning data and remaining gas.
	REVERT(source, length, cb)
	{
		this.stopped = 1
		cb()
	}

	// 0xFE BRK - D: 0 - A: 0 - Custom opcode for debugging. NOT PART OF THE ETHEREUM SPEC!
	BRK(cb)
	{
		this.interrupt = 1
		cb()
	}

	// 0xFF SELFDESTRUCT - D: 1 - A: 0 - Halt execution and register account for later deletion.
	SELFDESTRUCT(address, cb)
	{
		cb(new Error("Unfinished instruction: SELFDESTRUCT"))
	}

	//----------------------------------------------------------------------------------------------------------
	//
	// Gas helpers
	//
	//----------------------------------------------------------------------------------------------------------
	ceil32(n)
	{
		n = Number(n)
		return (n & ~0x1F) + (n & 0x1F ? 0x20 : 0);
	}

	// Memory fee algorithm
	memoryFee(size)
	{
		size = Number(size)
		let fee = ((size * gas.GMEMORY) + ((size * size) / gas.GQUADRATICMEMDENOM) | 0)
		return fee
	}

	// Returns the difference between the old fee and the new fee after the memory has grown in size
	calculateMemoryFee(oldSize, newSize)
	{
		if(newSize > oldSize)
		{
			oldSize = this.ceil32(oldSize) >> 5
			newSize = this.ceil32(newSize) >> 5
			let oldFee = this.memoryFee(oldSize)
			let newFee = this.memoryFee(newSize)
			return newFee - oldFee
		}
		return 0
	}

	copyDataFee(size)
	{
		if(size instanceof EtherValue) size = size.value()
		return gas.GCOPY * (this.ceil32(size) >> 5)
	}
}

//---------------------------------------------------------------------------------------------------------------------
//
// Module exports
//
if(module)
{
	module.exports.EthereumVirtualMachine = EthereumVirtualMachine
	module.exports.EtherValue = EtherValue
	module.exports.EtherMemory = EtherMemory
	//module.exports.EtherStack = EtherStack
}
