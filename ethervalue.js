//----------------------------------------------------------------------------------------------------------------------
//
// EtherValue
//
// This class is a wrapper around JS' bignum, so make it more Ethereum VM friendly, and to allow for
// eaiser tracking of the source of the value.
//
// Author: John Short <john@short.digital>
// Copyright © 2017 - 2019
// License: GPLv3
//
//----------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
//
// EtherValue
//
//---------------------------------------------------------------------------------------------------------------------
// For keeping track of the origin of every value on the stack
// The idea is that you should be able to follow a single bit from its point of creation
// to the point where it dissapears from existence by following the chain of parents / children
function reverseBuffer(buffer)
{
	for(let i = 0, j = buffer.length - 1; i < j; ++i, --j)
	{
		let t = buffer[j]
		buffer[j] = buffer[i]
		buffer[i] = t
	}
	return buffer
}
class EtherValue
{
	constructor(val, evm)
	{
		this.setValue(val)
		this.evm = evm // For keeping track of execution time
		this.id = -1
		if(!evm) throw new Error('Missing EVM Enviornment')
		this.pc = evm.curPc
		this.tick = evm.tick
		this.children = []
		this.parents = []
		this.operation = null
		this.created = -1
		this.died = -1
		this.copy = null // Circular references nessitates that the entire tree is cloned at once, thus all values must know who their clone is until everything's cloned
		if(evm && evm.addData)
			evm.addData(this)
	}

	setValue(val)
	{
		if(typeof val === 'string')
			val = '0x'+val //parseInt(val, 16) // bigInt(val, 16)
		else if(val instanceof Buffer)
			val = '0x'+val.toString('hex') //bigInt(val.toString('hex'), 16)
		else if(val instanceof EtherValue)
			val = val.val //bigInt(val.val)
		else if(val === null)
			val = 0
		else if(val === undefined)
			val = 0
		else
			val = val //bigInt(val)
		val = BigInt(val)
		val &= 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFn
		//val = val.and(bigInt('FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', 16)) // Cannot allow a value to be > 2^256 - 1
		this.val = val
	}

	value()
	{
		return this.val //.valueOf()
	}

	toArray(size)
	{
		size = Number(size)
		let s = this.val.toString(16)
		let valBuf = Buffer.from((s.length & 1 ? '0' : '')+s, 'hex')
		if(size)
		{
			let buf = Buffer.alloc(size)
			let bufLen = valBuf.length
			for(let i = 0; i < size; i++)
			{
				if(i >= size - bufLen)
					buf[i] = valBuf[i - (size - bufLen)]
				else
					buf[i] = 0
			}
			return buf
		}
		else
			return valBuf
	}

	toString(size)
	{
		if(!size)
			return this.val.toString(16)
		else
		{
			let s = new Array(size+1).join(0)+this.val.toString(16)
			return s.substring(s.length - size)
		}
	}

	unresialize(obj)
	{
		if(o.val)
			this.val = BigInt(o.val) //bigInt(o.val, 16)
		if(o.id)
			this.id = id
		if(this.evm.enableDataTrace)
		{
			if(this.parents.length)
			{
				this.parents = []
				for(let i in o.parents)
					this.parents[i] = o.parents[i]
			}
			if(this.children.length)
			{
				this.children = []
				for(let i in this.children)
					this.children[i] = o.children[i]
			}
		}
		if(o.operation)
			this.operation = o.operation
		if(o.created != -1)
			this.created = o.created
		if(o.died != -1)
			this.died = o.died
	}

	serialize()
	{
		let o = {
			id: this.id,
			val: this.val.toString(16),
		}
		if(this.evm.enableDataTrace)
		{
			if(this.parents.length)
			{
				o.parents = []
				for(let i in this.parents)
					o.parents[i] = this.parents[i]
			}
			if(this.children.length)
			{
				o.children = []
				for(let i in this.children)
					o.children[i] = this.children[i]
			}
		}
		if(this.operation)
			o.operation = this.operation
		if(this.created != -1)
			o.created = this.created
		if(this.died != -1)
			o.died = this.died
		return o
	}

	// Deep clone, 1:1
	// Should only be called on values without any parents!
	// (evm argument is for binding this value to a new EVM instance)
	clone(evm)
	{
		if(this.copy)
			return this.copy

		let v = new EtherValue(this, evm)
		this.copy   = v
		v.id        = this.id
		v.operation = this.operation
		v.created   = this.created
		v.died      = this.died
		v.pc        = this.pc
		v.tick      = this.tick

		if(this.evm.enableDataTrace)
		{
			// Clone all parents
			for(let i in this.parents)
				v.parents[i] = this.evm.dataTrace[this.parents[i]].clone(evm)

			// Clone all children
			for(let i in this.children)
			{
				v.children[i] = this.evm.dataTrace[this.children[i]].clone(evm)
			
				// Find out which index we have in our child's parents array
				for(let j in this.children[i].parents)
				{
					if(this.children[i].parents[j] == this.id)
						v.children[i].parents[j] = v
				}
			}
		}

		return v
	}

	// Remove all references to the clones
	// If we don't do this, we won't be able to clone the entire graph again!
	// Call this only once every value in the stack has been cloned
	finishCloning()
	{
		if(!this.copy) return
		this.copy = null
		
		if(this.evm.enableDataTrace)
		{
			for(let i in this.parents)
				if(this.parents[i].finishCloning)
					this.parents[i].finishCloning()
			for(let i in this.children)
				if(this.children[i].finishCloning)
					this.children[i].finishCloning()
		}
	}

	setOrigin(operation, parent1, parent2)
	{
		this.operation = operation
		if(this.evm.enableDataTrace)
		{
			if(parent1 && parent1.id != -1)
				this.parents.push(parent1.id)
			if(parent2 && parent2.id != -1)
				this.parents.push(parent2.id)
		}
		if(this.evm)
			this.created = this.evm.tick
	}

	addChild(child)
	{
		if(this.evm.enableDataTrace)
		{
			if(child.id != -1)
				this.children.push(child.id)
		}
	}

	size()
	{
		return Math.ceil(this.val.toString(16).length / 2)
	}

	// When values are poped from the stack
	kill()
	{
		if(this.evm)
			this.died = this.evm.tick
	}

	dup()
	{
		let n = new EtherValue(this.val, this.evm)
		n.setOrigin('dup', this)
		this.addChild(n)
		return n
	}

	add(right)
	{
		//let n = new EtherValue(this.val.add(right.val), this.evm)
		let n = new EtherValue(this.val + right.val, this.evm)
		n.setOrigin('add', this, right)
		this.addChild(n)
		right.addChild(n)
		return n
	}

	mul(right)
	{
		//let n = new EtherValue(this.val.multiply(right.val), this.evm)
		let n = new EtherValue(this.val * right.val, this.evm)
		n.setOrigin('mul', this, right)
		this.addChild(n)
		right.addChild(n)
		return n
	}

	sub(right)
	{
		//let n = new EtherValue(this.val.subtract(right.val), this.evm)
		let n = new EtherValue(this.val - right.val, this.evm)
		n.setOrigin('sub', this, right)
		this.addChild(n)
		right.addChild(n)
		return n
	}

	div(right)
	{
		//let n = new EtherValue(this.val.divide(right.val), this.evm)
		let lval = this.val
		let rval = right.val
		if(rval == 0)
		{
			rval = 1
			lval = 0
		}
		let n = new EtherValue(lval / rval, this.evm)
		n.setOrigin('div', this, right)
		this.addChild(n)
		right.addChild(n)
		return n
	}

	mod(right)
	{
		//let n = new EtherValue(this.val.mod(right.val), this.evm)
		let n = new EtherValue(this.val % right.val, this.evm)
		n.setOrigin('mod', this, right)
		this.addChild(n)
		right.addChild(n)
		return n
	}

	exp(exponent)
	{
		//let n = new EtherValue(this.val.pow(exponent.val), this.evm)
		let n = new EtherValue(this.val ** exponent.val, this.evm)
		n.setOrigin('exp', this, exponent)
		this.addChild(n)
		exponent.addChild(n)
		return n
	}

	signextend()
	{
		// :^(
	}

	lt(val)
	{
		let n = null
		let v = val
		if(val instanceof EtherValue)
			v = val.val
		//if(this.val.lt(v))
		if(this.val < v)
			n = new EtherValue(1, this.evm)
		else
			n = new EtherValue(0, this.evm)
		n.setOrigin('lt', this, val)
		this.addChild(n)
		val.addChild(n)
		return n
	}

	gt(val)
	{
		let n = null
		let v = val
		if(val instanceof EtherValue)
			v = val.val
		//if(this.val.gt(v))
		if(this.val > v)
			n = new EtherValue(1, this.evm)
		else
			n = new EtherValue(0, this.evm)
		n.setOrigin('gt', this, val)
		this.addChild(n)
		val.addChild(n)
		return n
	}

	eq(val)
	{
		let n = null
		let v = val
		if(val instanceof EtherValue)
			v = val.val
		//if(this.val.eq(v))
		if(this.val == v)
			n = new EtherValue(1, this.evm)
		else
			n = new EtherValue(0, this.evm)
		n.setOrigin('eq', this, val)
		this.addChild(n)
		if(val instanceof EtherValue)
		{
			val.addChild(n)
		}
		return n
	}

	isZero()
	{
		let n = null
		//if(this.val.eq(0))
		if(this.val == 0)
			n = new EtherValue(1, this.evm)
		else
			n = new EtherValue(0, this.evm)
		n.setOrigin('iszero', this)
		this.addChild(n)
		return n
	}

	and(right)
	{
		//let n = new EtherValue(this.val.and(right.val), this.evm)
		let n = new EtherValue(this.val & right.val, this.evm)
		n.setOrigin('and', this, right)
		this.addChild(n)
		right.addChild(n)
		return n
	}

	or(right)
	{
		//let n = new EtherValue(this.val.or(right.val), this.evm)
		let n = new EtherValue(this.val | right.val, this.evm)
		n.setOrigin('or', this, right)
		this.addChild(n)
		right.addChild(n)
		return n
	}

	xor(right)
	{
		//let n = new EtherValue(this.val.xor(right.val), this.evm)
		let n = new EtherValue(this.val ^ right.val, this.evm)
		n.setOrigin('xor', this, right)
		this.addChild(n)
		right.addChild(n)
		return n
	}

	not()
	{
		//let n = new EtherValue(this.val.not(), this.evm)
		let n = new EtherValue(this.val ^ 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFn, this.evm)
		n.setOrigin('not', this)
		this.addChild(n)
		return n
	}

	byte(offset)
	{
		let a = this.val.toArray(256)
		let n = new EtherValue(a[offset], this.evm)
		n.setOrigin('byte', this, offset)
		this.addChild(n)
		right.addChild(n)
		return n
	}
}

module.exports = EtherValue
