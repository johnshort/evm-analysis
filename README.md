Ethereum Virtual Machine
========================

This is a virtual machine for running Ethereum smart contracts in a controlled enviornment.

This project is not very user friendly. It's meant for developers and researchers who are very familiar with Ethereum assembly, and are looking for a virtual machine where they can inspect the machine state at any given time. It's meant to be modified and hacked to suit your needs, not to be used as a finished product.

The nice thing about this project is that it allows you to tweak every aspect of the execution enviornment.

There are a few opcodes that have not been implemented, but they're not used terribly often. I'll implement them whenever I see them actually being used.

Author
======

John Short <john@short.digital>
Copyright © 2017 - 2019

License
=======

GPLv3
