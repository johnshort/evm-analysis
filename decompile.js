//----------------------------------------------------------------------------------------------------------------------
//
// EVM Analysis - Decompiler
//
// The decompiler works by creating an execution graph, and using the object trace from the VM to abstract the
// execution into a high-level (pseudo, for now) language
//
// We cannot detect purely dynamic jumps.
// (E.g., if the jump address is pushed from CALLDATA, then we cannot make the jump)
//
// Note: This decompiler is still very unfinished. Don't expect any decent results except from the simplest of contracts.
//
// Author: John Short <john@short.digital>
// Copyright © 2017 - 2019
// License: GPLv3
//
//----------------------------------------------------------------------------------------------------------------------

const fs = require('fs')
const EVM = require('./vm2.js')

let contractFile = process.argv[2]

let contract = fs.readFileSync(contractFile)

let vm = createNewVM(contract)
let prevJump = -1
let firstJump = -1
let jumpLog = []
let executionLog = {}
let dynamicJumps = {}

let opReuse = {}
let varIndex = 0
let variables = {}

function createNewVM(contract)
{
	let vm = new EVM.EthereumVirtualMachine()
	vm.setCode(contract)
	vm.enableExecutionTrace = true
	vm.enableDataTrace = true
	return vm
}

function getDataGraph(entry)
{
	let r = {
		op: entry.operation,
		res: '0x'+entry.val.toString(16),
		pc: entry.pc,
		tick: entry.tick
	}

	// Is res a string?
	if(r.res.length == 66)
	{
		let b = Buffer.from(r.res.substring(2), 'hex')
		let t = true
		let terminatorLoc = -1
		for(let i = 0; i < b.length; i++)
		{
			if(terminatorLoc == -1 && b[i] == 0x00)
			{
				terminatorLoc = i
			}
			else if(terminatorLoc != -1 && b[i] != 0x00)
			{
				t = false
				break
			}
			else if(terminatorLoc == -1 && (b[i] < 0x20 || b[i] > 0x7F))
			{
				t = false
				break
			}
		}
		if(t)
		{
			if(terminatorLoc == -1) terminatorLoc = b.length
			let str = b.slice(0, terminatorLoc).toString('utf8')
			r.res = '"'+str+'"'
		}
	}

	if(entry.parents.length > 0)
	{
		r.args = []
		for(let i in entry.parents)
		{
			r.args.push(getDataGraph(entry.evm.dataTrace[entry.parents[i]]))
		}
	}
	return r
}

function convert(graph, depth)
{
	try
	{
		if(!depth) depth = 0
		let res = ''
		let ps = depth > 0 ? '(' : ''
		let pe = depth > 0 ? ')' : ''

		if(!graph)
		{
			throw new Error('Missing graph at depth '+depth)
		}

		if(graph.op == 'push')
		{
			if(!graph.res)
			{
				console.log(graph)
				throw new Error('Missing res depth '+depth)
			}
			return graph.res
		}

		let ops = {
			'add':            '{0} + {1}',
			'mul':            '{0} * {1}',
			'sub':            '{0} - {1}',
			'div':            '{0} / {1}',
			//'sdiv':            '{0} / {1}',
			'mod':            '{0} % {1}',
			//'smod':           '{0} % {1}',
			//'addmod':         '',
			//'mulmod':         '',
			'exp':            'pow({+0}, {+1})',
			//'signextent':     '',

			'lt':             '{0} < {1}',
			'gt':             '{0} > {1}',
			//'slt':             '{0} < {1}',
			//'sgt':             '{0} > {1}',
			'eq':             '{0} == {1}',
			'iszero':         '{0} == 0',
			'and':            '{0} & {1}',
			'or':             '{0} | {1}',
			'xor':            '{0} ^ {1}',
			'not':            '~{0}',
			//'byte':           '',
			'hash':           'sha3({+0}, {+1})', // sha3(memory_location, size)

			
			'address':        'address()',
			'balance':        'balance({0})',
			'origin':         'origin()',
			'caller':         'caller()',
			'callvalue':      'callvalue()',
			'calldataload':   'calldataload({+0})',
			'calldatasize':   'calldatasize()',
			//'calldatacopy':   'calldatacopy({0}, {1}, {2})', // (destination, source, length)
			'codesize':       'codesize()',
			//'codecopy':       'codecopy({0}, {1}, {2})', // (destination, source, length)
			'gasprice':       'gasprice()',
			'extcodesize':    'extcodesize({0})',
			//'extcodecopy':   'extcodecopy({0}, {1}, {2}, {3})', // (address, destination, source, length)
			'returndatasize': 'returndatasize()',
			//'returndatacopy':   'returndatacopy({0}, {1}, {2})', // (destination, source, length)

			'blockhash':      'blockhash()',
			'coinbase':       'coinbase()',
			'timestamp':      'timestamp()',
			'number':         'number()',
			'difficulty':     'difficulty()',
			'gaslimit':       'gaslimit()',

			'mload':          'memory[{+0}]',
			'sload':          'load({+0})',
			'pc':             'pc()',
			'msize':          'msize()',
			'gas':            'gas()',
			'dup':            '{-0}', // convert arg without incrementing depth

			'call':           'call_return_value'
		}

		let op = ops[graph.op]

		if(!op)
		{
			throw new Error('Unknown operation '+graph.op)
		}

		let args = op.match(/\{-?\+?\d+\}/g)
		let argCount = args ? args.length : 0

		if(argCount > 0 && (!graph.args || graph.args.length != argCount))
		{
			let givenArgs = graph.args ? graph.args.length : 0
			throw new Error('Arg mismatch: '+givenArgs+' != '+argCount+' for op: '+op)
		}

		for(let i = 0; i < argCount; i++)
		{
			// Have we seen this arg before?
			if(!opReuse[graph.args[i].tick])
			{
				// No. Tag it
				opReuse[graph.args[i].tick] = {seen: 1, arg: graph.args[i]}
			}
			else
			{
				// Yes. Convert to variable
				opReuse[graph.args[i].tick].seen++
				variables[varIndex++] = opReuse[graph.args[i].tick]
			}

			let argSymbol = args[i]
			let newDepth = depth + 1
			if(argSymbol[1] == '-')
				newDepth = depth
			if(argSymbol[1] == '+')
				newDepth = 0
			if(graph.args[i].op == 'push')
				newDepth = 0
			let arg = convert(graph.args[i], newDepth)
			op = op.replace(argSymbol, arg)
		}

		if(depth > 1)
			op = '('+op+')'
		return op
	}
	catch(e)
	{
		console.log('---- '+depth)
		console.log(graph)
		throw e
	}
}

function getStackGraph(offset)
{
	return getDataGraph(vm.dataTrace[vm.stack.head(offset).id])
}

function getGraphs(stack)
{
	let g = []
	for(let i in stack)
		g.push(getDataGraph(stack[i]))
	return g
}

let code = {}
let jumps = {}
let destinations = {}
let branches = []
let states = {}
let statesArr = []
//let code = ''
let stopcount = 0

function analyseLoop(cb)
{
	if(vm.stopped)
		return cb()

	if(!executionLog[vm.pc])
		executionLog[vm.pc] = 0
	executionLog[vm.pc]++

	if(executionLog[vm.pc] >= 10)
	{
		console.log('Seen this location '+executionLog[vm.pc]+' times')
		return cb()
	}

	vm.peekNextInstruction((err, op) =>
	{
		let mnemonic = vm.opcodeToMnemonic(op)
		let o = {
			tick: vm.tick,
			pc: vm.pc,
			op: op,
			mnemonic: mnemonic,
			stack: [],
			template: ''
		}

		let skip = false

		if(vm.jumped && mnemonic != 'JUMPDEST')
		{
			console.log('Illegal jump')
			return cb()
		}

		// If it's a write or branch instruction
		switch(mnemonic)
		{
			case 'CALL': // CALL(gas, address, value, in_, insize, out, outsize)
				o.template = '\tcall({0}, {1}, {2}, {4}, {5}, {6}, {7})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'SELFDESTRUCT':
				o.template = '\tselfdestruct({0})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'STATICCALL':
			case 'CALLBLACKBOX':
			case 'DELEGATECALL':
			case 'CALLCODE':
			case 'CREATE':
				throw new Error('fixme')

			//
			// Dumbass logging functions
			//
			case 'LOG0':
				// (a, b, cb)
				o.template = '\tlog({0}, {1})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'LOG1':
				// (a, b, c, cb)
				o.template = '\tlog({0}, {1}, {2})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'LOG2':
				// (a, b, c, d, cb)
				o.template = '\tlog({0}, {1}, {2}, {3})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'LOG3':
				// (a, b, c, d, e, cb)
				o.template = '\tlog({0}, {1}, {2}, {3}, {4})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'LOG4':
				// (a, b, c, d, e, f, cb)
				o.template = '\tlog({0}, {1}, {2}, {3}, {4}, {5})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			//
			//
			//
			case 'REVERT':
				o.template = '\trevert({0}, {1})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'STOP':
				o.template = '\tstop()'
				break

			//
			//
			//
			case 'JUMP':
				o.stack.push(getStackGraph(0))
				if(!jumps[vm.pc])
					jumps[vm.pc] = []
				jumps[vm.pc].push(o)
				o.template = '\tgoto loc_{0}'
				break

			//
			//
			//
			case 'JUMPI':

				// Have we visited this jump before?
				if(dynamicJumps[vm.pc] === undefined)
				{
					// No. Take the jump.
					dynamicJumps[vm.pc] = true
				}

				// Save the PC we jumped at
				prevJump = vm.pc
				jumpLog.push(vm.pc)

				// Save the first jump?
				if(firstJump == -1)
					firstJump = vm.pc

				// Force jump
				if(dynamicJumps[vm.pc])
					vm.stack.head(1).setValue(1)
				else
					vm.stack.head(1).setValue(0)

				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				o.template = '\tif({1}) goto loc_{0}'
				break

			case 'SSTORE':
				o.template = '\tstore({0}, {1})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'MSTORE':
				o.template = '\tmemory[{0}] = {1}'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'RETURN':
				o.template = '\treturn({0}, {1})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'JUMPDEST':
				if(!destinations[vm.pc])
					destinations[vm.pc] = 0
				destinations[vm.pc]++
				o.template = '\n{pc}:'
				break

			case 'CALLDATACOPY':
				o.template = '\tcalldatacopy({0}, {1}, {2})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'CODECOPY':
				o.template = '\tcalldatacopy({0}, {1}, {2})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'EXTCODECOPY':
				o.template = '\tcalldatacopy({0}, {1}, {2}, {3})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			case 'RETURNDATACOPY':
				o.template = '\treturndatacopy({0}, {1}, {2})'
				o.stack = getGraphs(vm.getOpcodeStackItems(op))
				break

			default:
				skip = true
		}

		if(!skip)
		{
			if(!code[vm.pc])
				code[vm.pc] = []
			code[vm.pc].push(o)
		}

		// Execute the opcode
		vm.step((err, res) =>
		{
			if(err)
			{
				console.log(vm.stack.print())
				throw err
			}
			setImmediate(() =>
			{
				analyseLoop(cb)
			})
		})
	})
}

function compile(code)
{
	let i
	let r = ''
	let output = {}
	try
	{
		for(i in code)
		{
			let instruction = code[i][0]
			let c = instruction.template
			for(let i in instruction.stack)
			{
				c = c.replace('{'+i+'}', convert(instruction.stack[i], 0))
			}
			if(!c) console.log(instruction)
			if(c.indexOf('{pc}') != -1)
				c = c.replace('{pc}', 'loc_0x'+instruction.pc.toString(16))
			else
				c += ';'
			output[i] = c
			//r += c + '\n'
		}
		return output
	}
	catch(e)
	{
		console.log('---Error---')
		//console.log('code')
		//console.log(code)
		console.log('r:')
		console.log(r)
		console.log('code[i]')
		console.log(code[i])
		console.log('code[i][0]')
		console.log(code[i][0])
		throw e
	}
}

function areAllLinesEqual(lines)
{
	let diffs = 0
	for(let i in lines)
	{
		let cline = lines[i]
		for(let j in lines)
		{
			if(cline != lines[j])
				diffs++
		}
	}
	return diffs
}

function restructure(code)
{	
	let mergedCode = ''

	for(let i in code)
	{
		// Must find a way to handle cases where an execution path crosses with other paths,
		// leading to a situation where we get widely different results from the object trace
		let diffs = areAllLinesEqual(code[i])
		if(diffs == 0)
		{
			mergedCode += code[i][0] + '\n'
		}
		else
		{
			mergedCode += code[i][0] + ' // ==== FIXME '+diffs+' ====\n'
		}
	}

	let lines = mergedCode.split('\n')

	// Find locations
	let blocks = {}
	for(let i in lines)
	{
		let line = lines[i]
		let tline = line.trim()
		let m = tline.match(/goto (.*?);/)
		if(m)
			blocks[m[1]] = {code: [], entries: [], exits: []}
		m = tline.match(/^(.*?):$/)
		if(m)
			blocks[m[1]] = {code: [], entries: [], exits: []}
	}

	// Find block code & exits
	let curBlockLoc = null
	let curBlock = null
	for(let i in lines)
	{
		let line = lines[i]
		let tline = line.trim()

		// Entries/Exits
		let m = tline.match(/goto (.*?);/)
		if(m)
		{
			blocks[m[1]].entries.push(curBlockLoc)
			if(curBlock)
				curBlock.exits.push(m[1])
		}

		// New block
		if(line[line.length - 1] == ':')
		{
			let newBlockLoc = line.substring(0, line.length - 1)
			let newBlock = blocks[newBlockLoc]
			if(!newBlock)
			{
				console.log(newBlockLoc)
				throw new Error('???')
			}

			// Will the previous block blow into this block?
			if(curBlock)
			{
				let ll = curBlock.code[curBlock.code.length - 1]
				if(ll && ll.match(/^(goto|stop|revert)/) == null)
				{
					// Yes, it will
					curBlock.code.push('goto '+newBlockLoc+';')
					curBlock.exits.push(newBlockLoc)
					newBlock.entries.push(curBlockLoc)
				}
			}
			curBlock = newBlock
			curBlockLoc = newBlockLoc
		}
		// Regular old code
		else
		{
			let tline = line.trim()
			if(tline == '' || tline == '\n')
				continue
			if(curBlock)
				curBlock.code.push(tline)
		}
	}

	console.log(blocks)

	// Merge blocks
	console.log('==== MERGE ====')
	let doMerge = true
	while(doMerge)
	{
		doMerge = false
		for(let i in blocks)
		{
			let block = blocks[i]
			if(block.exits.length == 1)
			{
				if(blocks[block.exits[0]].entries.length == 1)
				{
					// There blocks are connected, with no loose ends. Let's join them
					let joinBlockLoc = block.exits.pop() 
					let join = blocks[joinBlockLoc]
					console.log('Merge '+i+' and '+joinBlockLoc)
					block.code.pop()
					for(let j in join.code)
						block.code.push(join.code[j])
					block.exits = join.exits
					delete blocks[joinBlockLoc]
					doMerge = true
					break
				}
			}
		}
	}
	console.log('==== MERGE END ====')
	console.log(blocks)

	console.log(destinations)

	let c = ''
	let locs = Object.keys(blocks).sort()
	for(let i in locs)
	{
		let loc = locs[i]
		let block = blocks[loc]
		c += '\n' + loc + ':\n'
		for(let j in block.code)
			c += '\t' + block.code[j] + '\n'
	}
	console.log(c)

	//console.log(lines)
}

//vm.calldata.write(0, new Buffer('', 'hex'), 68, () => {})
//vm.setCallvalue(10000000000)
let count = 0
let maxCount = 1000
let prevCompiledCode = ''
let combinedCode = {0: ['entry_point:']}
let codePerRun = []
function doLoop()
{
	console.log('Pass '+count)
	if(count++ == maxCount) return
	analyseLoop(() =>
	{
		let compiledCode = compile(code)
		codePerRun.push(code)
		code = {}
		opReuse = {}
		
		// Combine compiled code
		let c = ''
		for(let i in compiledCode)
		{
			if(combinedCode[i] === undefined)
				combinedCode[i] = []
			if(compiledCode[i] != combinedCode[i][0])
				combinedCode[i].push(compiledCode[i])
		}

		// Find previous jump not taken
		let found = false
		for(let i = jumpLog.length - 1; i >= 0; i--)
		{
			if(dynamicJumps[jumpLog[i]])
			{
				dynamicJumps[jumpLog[i]] = false
				found = true
				break
			}
		}
		
		// Are we done?
		if(!found)
		{
			// We have taken all jumps...
			console.log('We have explored all paths')
			fs.writeFileSync('state_analysis.json', JSON.stringify(codePerRun), 'utf8')
			//restructure(combinedCode)
			return
		}

		// Reset state
		jumpLog = []
		executionLog = {}

		// Create new VM
		vm = createNewVM(contract)

		// Restart analysis
		setTimeout(() =>
		{
			doLoop()
		}, 0)
	})
}
doLoop()
