//----------------------------------------------------------------------------------------------------------------------
//
// EVM Analysis
//
// This is the main entry of the tool.
// Use this to cobble together an enviornment for the contract you want to analyse.
//
// Author: John Short <john@short.digital>
// Copyright © 2017 - 2019
// License: GPLv3
//
//----------------------------------------------------------------------------------------------------------------------

const Web3 = require('web3')
const fs = require('fs')
const EVM = require('./vm2.js')
const Web3LiveStorage = require('./web3livestorage.js')
const ethPrice = 156.86

let instructionExecutedCount = 0
let startTime = Date.now()

function customTraceStats(cb)
{
	const cpuclock = 4194300
	let trace = ' - '
	vm.memory.read(32, 32, (err, res) =>
	{
		let cycleCount = res.readUInt32BE(28)
		trace += cycleCount + ' ('+((cycleCount*(1/1000000))*1000).toFixed(4)+' ms)'

		trace += ' ('+(instructionExecutedCount/((Date.now() - startTime) / 1000))+' i/s)'
		instructionExecutedCount++

		cb(null, trace)
	})
}

function printHex(n)
{
	var h = "0123456789ABCDEF";
	return h[(n >> 4)&0xF]+h[n&0xF];
}
function printHex16(n)
{
	return printHex((n>>8)&0xFF)+printHex(n&0xFF);
}
function printHex32(n)
{
	return printHex16((n>>16)&0xFFFF)+printHex16(n&0xFFFF);
}

function drawPaddedRight(str, size, padding)
{
	if(str.length < size)
		return str+(new Array(size - str.length + 1).join(padding))
	return str
}

function renderVMState(evm, cb)
{
	var lines = []
	var firstLine = ''
	firstLine += drawPaddedRight("│ [MEMORY] (Fee: "+evm.memoryFee(evm.memory.size())+')', 72, ' ')
	firstLine += ' │ '
	firstLine += drawPaddedRight("[STACK]", 83, ' ')
	firstLine += ' │ '
	lines.push(firstLine)
	lines.push(
		'├'+
			drawPaddedRight('', 72, '─')+
			'┼'+
			drawPaddedRight('', 85, '─')+
		'┤')
	for(var i = 0; i < 0x1A; i++)
	{
		var line = ''

		// Memory
		var memLine = '│ '
		memLine += printHex16(i * 0x20)
		memLine += ': '
		var memsize = evm.memory.size()
		var absI = 0;
		if(i*32 < memsize)
		{
			for(var j = 0; (j < 32) && (absI < memsize); j++, absI++)
				memLine += printHex(evm.memory.mem[(i*32)+j])
		}
		line += drawPaddedRight(memLine, 72, ' ')
		line += ' │ '

		// Stack
		var stackLine = ''
		stackLine += printHex16(i).substr(-3)
		stackLine += ': '
		var stacksize = evm.stack.stack.length
		if(i < stacksize)
		{
			var v = evm.stack.stack[i].toArray(32)
			//console.log(v)
			for(var j = 0; j < 32; j++)
				stackLine += printHex(v[j] === undefined ? 0 : v[j])
			stackLine += ' ('+printHex16(evm.stack.stack[i].created)+', '+evm.stack.stack[i].operation+')'
		}
		line += drawPaddedRight(stackLine, 83, ' ')
		line += ' │ '

		lines.push(line)
	}
	lines.push('├'+drawPaddedRight('', 158, '─')+'┤')

	customTraceStats((err, customTrace) =>
	{
		let traceStats = '(Fee: '+evm.accumulatedGas+' ($'+((evm.accumulatedGas*0.000000001)*ethPrice).toFixed(6)+')'+') - PC: '+printHex32(evm.pc-1)+' - Tick: '+evm.tick
		traceStats += customTrace
		lines.push(drawPaddedRight('│ [TRACE] '+traceStats, 159, ' ')+'│')

		lines.push('├'+drawPaddedRight('', 158, '─')+'┤')
		for(var i = 0; i < 10; i++)
		{
			var line = ''
			// Trace
			var traceLine = '│ '

			var start = evm.objTrace.length - 1 - i
			if(start >= 0)
			{
				var traceObj = evm.objTrace[start]
				traceLine += printHex32(traceObj.pc)
				traceLine += ': '
				traceLine += printHex(traceObj.opcode)
				traceLine += ' '
				traceLine += traceObj.mnemonic
				traceLine += ' '

				if(traceObj.consumedStack.length)
					traceLine += '>('+traceObj.consumedStack.map(function(i){return i.val.toString(16);}).join(', ')+')';
				if(traceObj.consumedStack.length && traceObj.addedStack.length)
					traceLine += ' -> ';
				if(traceObj.addedStack.length)
					traceLine += '('+traceObj.addedStack.map(function(i){return i.val.toString(16);}).join(', ')+')>';
			}
			line += drawPaddedRight(traceLine, 159, ' ')+'│'
			lines.push(line)
		}
		lines.push('└'+drawPaddedRight('', 158, '─')+'┘')

		return cb(null, lines.join('\n'))	
	})
}

function stripComments(file)
{
	let multilineStart = -1
	let inString = false
	let inMultiline = false
	let inSingleline = false
	file = file.split('')
	let len = file.length
	for(let i = 0; i < len; i++)
	{
		if(!inMultiline && !inSingleline && !inString)
		{
			// Multiline
			if(file[i] == '/' && file[i+1] == '*')
			{
				inMultiline = true
				multilineStart = i
			}
			// Single line
			if(file[i] == '/' && file[i+1] == '/')
			{
				inSingleline = true
				multilineStart = i
			}
			// String
			if(file[i] == '"')
			{
				inString = true
			}
		}
		else if(inSingleline)
		{
			if(file[i] == '\n')
			{
				inSingleline = false
				for(let j = multilineStart; j < i; j++)
					file[j] = ' '
			}
		}
		else if(inString)
		{
			if(file[i] == '"' && file[i-1] != '\\')
				inString = false
		}
		else if(inMultiline)
		{
			if(file[i] == '*' && file[i+1] == '/')
			{
				inMultiline = false
				for(let j = multilineStart; j < i + 2; j++)
					file[j] = ((file[j] == '\n') ? '\n' : ' ')
			}
		}
	}
	return file.join('')
}

function parseJSON(file)
{
	file = stripComments(file)
	return JSON.parse(file)
}

function readStateFile(vm, stateFilename)
{
	let state = parseJSON(fs.readFileSync(stateFilename, 'utf8'))

	// Get settings
	if(state.settings)
	{
		if(state.settings.liveStorage == true)
			vm.setStorage(new Web3LiveStorage())
	}

	// Load any pre-defined storage
	if(state.storage)
		vm.storage.load(state.storage)

	// Get the contract code
	if(state.contractCodeFile)
	{
		let contract = fs.readFileSync(state.contractCodeFile)
		vm.setCode(contract)
	}

	// Set the contract address
	if(state.contractAddress)
		vm.setContractAddress(state.contractAddress.toLowerCase())

	console.log(state.contractAddress.toLowerCase())
}

function readCallFile(vm, callFilename, cb)
{
	let call = parseJSON(fs.readFileSync(callFilename, 'utf8'))

	//console.log(new Buffer(call.calldata.join(''), 'hex'))

	// Load the state, if any
	if(call.state)
		readStateFile(vm, call.state)

	// Set caller and origin
	if(call.caller)
		vm.setCaller(call.caller)
	if(call.origin)
		vm.setOrigin(call.origin)
	else
		vm.setOrigin(call.caller)

	// Set value
	if(call.value)
		vm.setCallvalue(call.value)

	// Set calldata
	if(call.calldata)
	{
		let calldata = null
		if(call.calldata instanceof Array)
			calldata = Buffer.from(call.calldata.join(''), 'hex')
		else
			calldata = Buffer.from(call.calldata, 'hex')
		vm.setCalldata(calldata)
	}

	// Set block info
	if(call.block)
	{
		let block = call.block
		if(block.live)
		{
			let web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/'))
			web3.eth.getBlock(block.number, (err, res) =>
			{
				vm.setBlock(res)
				//vm.storage.setBlock(res.number - 1)
			})
		}
		else
		{
			vm.setBlock(block)
			cb(null, null)
		}
	}
	else
		cb(null, null)
}

// All calls need new VMs
let vm = new EVM.EthereumVirtualMachine()

let callFile = process.argv[2]
if(callFile)
{
	readCallFile(vm, callFile, (err, res) =>
	{

	})
}
else
{
	console.log("Needs a callfile.json")
	return
}

let instructionCount = 0
function maybeRenderState(cb)
{
	instructionCount++
	if(stepping || (instructionCount % 20000) == 0)
	{
		renderVMState(vm, (err, vmstate) =>
		{
			console.log('\x1b[;H'+vmstate)
			console.log('  [s] Stack     [m] Memory     [t] Storage     [b] Breakpoints     [q] Quit')
			cb()
		})
	}
	else
		cb()
}

let stepsTaken = 0
let stepsReset = 300
function execute()
{
	vm.step((err, res) =>
	{
		if(err)
		{
			console.log(vm)
			console.log(vm.storage.storage)
			throw err
		}
		inStep = false

		vm.peekNextInstruction((err, nextOpcode) =>
		{
			if(err) throw err
			if(nextOpcode == 0xFE)
			{
				stepping = true
			}

			maybeRenderState(() =>
			{
				if(!stepping)
				{
					stepsTaken++
					if(stepsTaken >= stepsReset)
					{
						stepsTaken = 0
						//setTimeout(() =>
						process.nextTick(() =>
						{
							execute()
						})
					}
					else
					{
						execute()
					}
				}
			})
		})
	})
}

let stepping = false
let inStep = false
process.stdin.setRawMode(true)
process.stdin.resume()
process.stdin.on('data', function(d)
{
	if(!d) return
	if(d[0] == 0x71) // q
	{
		console.log("exit")
		console.log(JSON.stringify(vm.storage.storage, null, 4))
		console.log(vm.profiling)
		process.exit(0)
	}
	if(d[0] == 0x72) // r
	{
		stepping = false
	}

	if(inStep) return
	execute()
})
