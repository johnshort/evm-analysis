//----------------------------------------------------------------------------------------------------------------------
//
// Web3LiveStorage
//
// Fetch live storage values from Infura.
// All values are cached, and the cache can be overwritten by the contract.
//
// Author: John Short <john@short.digital>
// Copyright © 2017 - 2019
// License: GPLv3
//
//----------------------------------------------------------------------------------------------------------------------

const Web3 = require('web3')
const EtherValue = require('./ethervalue.js')

class Web3LiveStorage
{
	constructor(evm)
	{
		this.web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/'))
		this.storage = {}
		this.evm = evm
	}

	setBlock(number)
	{
		this.number = number
	}

	load(values)
	{
		for(let i in values)
			this.storage[i] = values[i]
	}

	setEvm(evm)
	{
		this.evm = evm
	}

	// Replace these on runtime.
	read(address, offset, cb)
	{
		if(this.storage[offset.toString()])
		{
			console.log(offset.toString())
			console.log(this.storage[offset.toString()])
			let n = new EtherValue(this.storage[offset.toString()], this)
			n.setOrigin('storage', offset)
			cb(null, n)
		}
		else
		{
			console.log('Addr:  0x'+address.toString(40))
			console.log('Index: 0x'+offset.toString(64))
			let callback = (err, res) =>
			{
				if(err) throw err
				console.log(res)
				this.storage[offset.toString()] = res.substring(2)
				cb(0, new EtherValue(res.substring(2)))
			}
			//if(this.number)
			//	this.web3.eth.getStorageAt('0x'+address.toString(40), '0x'+offset.toString(64), this.number, callback)
			//else
				this.web3.eth.getStorageAt('0x'+address.toString(40), '0x'+offset.toString(64), callback)
		}
	}

	write(address, offset, value, cb)
	{
		//cb(new Error("No storage"))
		this.storage[offset.toString()] = value.toString()
		cb()
	}
}

module.exports = Web3LiveStorage
